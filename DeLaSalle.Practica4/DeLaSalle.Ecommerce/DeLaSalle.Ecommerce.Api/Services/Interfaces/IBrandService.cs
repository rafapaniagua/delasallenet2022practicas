using DeLaSalle.Ecommerce.Core.Dto;

namespace DeLaSalle.Ecommerce.Api.Services.Interfaces;

public interface IBrandService
{
    Task<bool> BrandExist(int id);
    Task<BrandDto> SaveAsync(BrandDto brandDto);
    Task<BrandDto> UpdateAsync(BrandDto brandDto);
    Task<List<BrandDto>> GetAllAsync();
    Task<bool> DeleteAsync(int id);
    Task<BrandDto> GetById(int id);
}