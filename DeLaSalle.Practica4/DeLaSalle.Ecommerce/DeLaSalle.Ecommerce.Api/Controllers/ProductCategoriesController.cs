using DeLaSalle.Ecommerce.Api.Repositories.Interfaces;
using DeLaSalle.Ecommerce.Api.Services.Interfaces;
using DeLaSalle.Ecommerce.Core.Dto;
using DeLaSalle.Ecommerce.Core.Entities;
using DeLaSalle.Ecommerce.Core.Http;
using Microsoft.AspNetCore.Mvc;

namespace DeLaSalle.Ecommerce.Api.Controllers;

[ApiController]
[Route("api/[controller]")]
public class ProductCategoriesController : ControllerBase
{
    private readonly IProductCategoryService _productCategoryService;
    
    public ProductCategoriesController(IProductCategoryService productCategoryService)
    {
        _productCategoryService = productCategoryService;
    }

    [HttpGet]
    public async Task<ActionResult<Response<List<ProductCategoryDto>>>> GetAll()
    {
        // var lst = new List<ProductCategory>();
        //
        // lst.Add(new ProductCategory{ Id = 1, Name = "Test", Description = "Test"});
        // lst.Add(new ProductCategory{ Id = 2, Name = "Test2", Description = "Test2"});

        /*var response = new Response<List<ProductCategoryDto>>();
        
        var categories = await _productCategoryRepository.GetAllAsync();

        response.Data = categories.Select(c => new ProductCategoryDto(c)).ToList();

        return Ok(response);*/
        // return NotFound();

        var response = new Response<List<ProductCategoryDto>>
        {
            Data = await _productCategoryService.GetAllAsync()
        };

        return Ok(response);
    }

    [HttpPost]
    public async Task<ActionResult<Response<ProductCategoryDto>>> Post([FromBody] ProductCategoryDto categoryDto)
    {
        /*var response = new Response<ProductCategoryDto>();

        var category = new ProductCategory
        {
            Name = categoryDto.Name,
            Description = categoryDto.Description,
            CreatedBy = String.Empty,
            CreatedDate = DateTime.Now,
            UpdatedBy = String.Empty,
            UpdatedDate = DateTime.Now
        };
            
            
        category = await _productCategoryRepository.SaveAsync(category);
        categoryDto.Id = category.Id;

        response.Data = categoryDto;
        
        return Created($"/api/[controller]/{categoryDto.Id}",response);*/

        var response = new Response<ProductCategoryDto>
        {
            Data = await _productCategoryService.SaveAsync(categoryDto)
        };
        return Created($"/api/[controller]/{response.Data.Id}",response);
    }

    [HttpGet]
    [Route("{id:int}")]
    public async Task<ActionResult<Response<ProductCategoryDto>>> GetById(int id)
    {
        /*var response = new Response<ProductCategoryDto>();
        var category = await _productCategoryRepository.GetById(id);
        

        if (category == null)
        {
            response.Errors.Add("Product Category Not Found");
            return NotFound(response);
        }

        var categoryDto = new ProductCategoryDto(category);
        response.Data = categoryDto;
        
        return Ok(response);*/

        var response = new Response<ProductCategoryDto>();

        if (!await _productCategoryService.ProductCategoryExist(id))
        {
            response.Errors.Add("Product Category Not Found");
            return NotFound(response);
        }

        response.Data = await _productCategoryService.GetById(id);
        return Ok(response);
    }

    [HttpPut]
    public async Task<ActionResult<Response<ProductCategoryDto>>> Update([FromBody] ProductCategoryDto categoryDto)
    {
        /*var response = new Response<ProductCategoryDto>();
        var category = await _productCategoryRepository.GetById((categoryDto.Id));

        if (category == null)
        {
            response.Errors.Add("Product Category Not Found");
            return NotFound(response);
        }

        category.Name = categoryDto.Name;
        category.Description = categoryDto.Description;
        category.UpdatedBy = String.Empty;
        category.UpdatedDate = DateTime.Now;
        
        await _productCategoryRepository.UpdateAsync(category);

        response.Data = categoryDto;

        return Ok(response);*/
        
        var response = new Response<ProductCategoryDto>();

        if (!await _productCategoryService.ProductCategoryExist(categoryDto.Id))
        {
            response.Errors.Add("Product Category Not Found");
            return NotFound(response);
        }

        response.Data = await _productCategoryService.UpdateAsync(categoryDto);
        return Ok(response);
    }

    [HttpDelete]
    [Route("{id:int}")]
    public async Task<ActionResult<Response<bool>>> Delete(int id)
    {
        var response = new Response<bool>();
        
        if (!await _productCategoryService.ProductCategoryExist(id))
        {
            response.Errors.Add("Product Category Not Found");
            return NotFound(response);
        }

        var result = await _productCategoryService.DeleteAsync(id);
        response.Data = result;

        return Ok();
        
        /*var response = new Response<bool>();
        var result = await _productCategoryRepository.DeleteAsync(id);
        response.Data = result;

        return Ok();*/
    }
}