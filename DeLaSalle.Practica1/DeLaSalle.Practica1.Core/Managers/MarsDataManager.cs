using DeLaSalle.Practica1.Core.Entities;
using DeLaSalle.Practica1.Core.Managers.Interfaces;
using DeLaSalle.Practica1.Core.Services.Interfaces;

namespace DeLaSalle.Practica1.Core.Managers;

public class MarsDataManager : IMarsDataManager
{
    private readonly IMarsDataService _service;

    public MarsDataManager(IMarsDataService service)
    {
        _service = service;
    }

    public MarsData GetMarsData(Person person)
    {
        return _service.ProcessMarsData(person);
    }
}