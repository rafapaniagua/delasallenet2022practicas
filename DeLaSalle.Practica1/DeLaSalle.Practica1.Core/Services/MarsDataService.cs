using DeLaSalle.Practica1.Core.Entities;
using DeLaSalle.Practica1.Core.Services.Interfaces;

namespace DeLaSalle.Practica1.Core.Services;

public class MarsDataService : IMarsDataService
{
    public MarsData ProcessMarsData(Person person)
    {
        var marsData = new MarsData();
        marsData.MarsWeight = (float) Math.Round((person.Weight / 9.81f) * 3.69f, 1);

        return marsData;
    }
}