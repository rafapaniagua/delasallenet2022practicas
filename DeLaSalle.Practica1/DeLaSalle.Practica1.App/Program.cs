﻿using DeLaSalle.Practica1.Core.Entities;
using DeLaSalle.Practica1.Core.Managers;
using DeLaSalle.Practica1.Core.Services;
using System;

namespace DeLaSalle.Practica1.App;

public static class Program
{
    public static void Main(string[] args)
    {
        float weight = 0;

        System.Console.WriteLine("Enter your Weight in Earth [KG] :");
        Single.TryParse(System.Console.ReadLine(), out weight);

        var person = new Person{ Weight = weight };

        var service = new MarsDataService();
        var manager = new MarsDataManager(service);

        MarsData marsData = manager.GetMarsData(person);

        System.Console.WriteLine($"Your weight in Mars is {marsData.MarsWeight} kg");
    }
}