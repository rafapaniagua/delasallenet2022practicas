﻿using DeLaSalle.TaxCalculator.App.Taxes;
using DeLaSalle.TaxCalculator.Core.Entities;

namespace DeLaSalle.TaxCalculator.Tests;

public class ISRCalculatorServiceShould
{
    [Fact]
    public void GetISR_WhenSalaryAmount6578_55_Returns126_30797()
    {
        //arrange
        var expectedISR = 126.30797;
        var amount = 6578.55;
        var salary = new Salary{ Amount = amount};
        var sut = new ISRCalculatorService(); //SYSTEM UNDER TEST

        //act
        var result = sut.GetISR(salary);

        //assert
        Assert.Equal(expectedISR, result);
    }
    
    [Fact]
    public void GetISR_WhenSalaryAmount13500_00_Returns517_46936()
    {
        //arrange
        var expectedISR = 517.46936;
        var amount = 13500.00;
        var salary = new Salary{ Amount = amount};
        var sut = new ISRCalculatorService(); //SYSTEM UNDER TEST

        //act
        var result = sut.GetISR(salary);

        //assert
        Assert.Equal(expectedISR, result);
    }
    
    [Fact]
    public void GetISR_WhenSalaryAmount100000_00_Returns7592_30250()
    {
        //arrange
        var expectedISR = 7592.30250;
        var amount = 100000.00;
        var salary = new Salary{ Amount = amount};
        var sut = new ISRCalculatorService(); //SYSTEM UNDER TEST

        //act
        var result = sut.GetISR(salary);

        //assert
        Assert.Equal(expectedISR, result);
    }
    
    [Fact]
    public void GetISR_WhenSalaryAmount119457_20_Returns9918_20640()
    {
        //arrange
        var expectedISR = 9918.20640;
        var amount = 119457.20;
        var salary = new Salary{ Amount = amount};
        var sut = new ISRCalculatorService(); //SYSTEM UNDER TEST

        //act
        var result = sut.GetISR(salary);

        //assert
        Assert.Equal(expectedISR, result);
    }
    
    [Fact]
    public void GetISR_WhenSalaryAmount154111_23_Returns15846_69235()
    {
        //arrange
        var expectedISR = 15846.69235;
        var amount = 154111.23;
        var salary = new Salary{ Amount = amount};
        var sut = new ISRCalculatorService(); //SYSTEM UNDER TEST

        //act
        var result = sut.GetISR(salary);

        //assert
        Assert.Equal(expectedISR, result);
    }
    
    [Fact]
    public void GetISR_WhenSalaryAmount300000_00_Returns46786_08182()
    {
        //arrange
        var expectedISR = 46786.08182;
        var amount = 300000.00;
        var salary = new Salary{ Amount = amount};
        var sut = new ISRCalculatorService(); //SYSTEM UNDER TEST

        //act
        var result = sut.GetISR(salary);

        //assert
        Assert.Equal(expectedISR, result);
    }
    
    [Fact]
    public void GetISR_WhenSalaryAmount505474_99_Returns94598_38290()
    {
        //arrange
        var expectedISR = 94598.38290;
        var amount = 505474.99;
        var salary = new Salary{ Amount = amount};
        var sut = new ISRCalculatorService(); //SYSTEM UNDER TEST

        //act
        var result = sut.GetISR(salary);

        //assert
        Assert.Equal(expectedISR, result);
    }
    
    [Fact]
    public void GetISR_WhenSalaryAmount904512_45_Returns213987_17200()
    {
        //arrange
        var expectedISR = 213987.17200;
        var amount = 904512.45;
        var salary = new Salary{ Amount = amount};
        var sut = new ISRCalculatorService(); //SYSTEM UNDER TEST

        //act
        var result = sut.GetISR(salary);

        //assert
        Assert.Equal(expectedISR, result);
    }
    
    [Fact]
    public void GetISR_WhenSalaryAmount1234567_89_Returns318204_46200()
    {
        //arrange
        var expectedISR = 318204.46200;
        var amount = 1234567.89;
        var salary = new Salary{ Amount = amount};
        var sut = new ISRCalculatorService(); //SYSTEM UNDER TEST

        //act
        var result = sut.GetISR(salary);

        //assert
        Assert.Equal(expectedISR, result);
    }
    
    [Fact]
    public void GetISR_WhenSalaryAmount3000000_00_Returns917155_12300()
    {
        //arrange
        var expectedISR = 917155.12300;
        var amount = 3000000.00;
        var salary = new Salary{ Amount = amount};
        var sut = new ISRCalculatorService(); //SYSTEM UNDER TEST

        //act
        var result = sut.GetISR(salary);

        //assert
        Assert.Equal(expectedISR, result);
    }
    
    [Fact]
    public void GetISR_WhenSalaryAmount10500600_85_Returns3533384_01200()
    {
        //arrange
        var expectedISR = 3533384.01200;
        var amount = 10500600.85;
        var salary = new Salary{ Amount = amount};
        var sut = new ISRCalculatorService(); //SYSTEM UNDER TEST

        //act
        var result = sut.GetISR(salary);

        //assert
        Assert.Equal(expectedISR, result);
    }
}