using DeLaSalle.Ecommerce.Api.Repositories.Interfaces;
using DeLaSalle.Ecommerce.Core.Dto;
using DeLaSalle.Ecommerce.Core.Entities;
using DeLaSalle.Ecommerce.Core.Http;
using Microsoft.AspNetCore.Mvc;

namespace DeLaSalle.Ecommerce.Api.Controllers;

[ApiController]
[Route("api/[controller]")]
public class BrandController : ControllerBase
{
    private readonly IBrandRepository _brandRepository;
    
    public BrandController(IBrandRepository brandRepository)
    {
        _brandRepository = brandRepository;
    }
    
    [HttpGet]
    public async Task<ActionResult<Response<List<Brand>>>> GetAll()
    {
        var response = new Response<List<Brand>>
        {
            Data = await _brandRepository.GetAllAsync()
        };

        return Ok(response);
    }
    
    [HttpPost]
    public async Task<ActionResult<Response<Brand>>> Post([FromBody] Brand brand)
    {
        var response = new Response<Brand>
        {
            Data = await _brandRepository.SaveAsync(brand)
        };
        return Created($"/api/[controller]/{response.Data.Id}",response);
    }
    
    [HttpGet]
    [Route("{id:int}")]
    public async Task<ActionResult<Response<Brand>>> GetById(int id)
    {
        var response = new Response<Brand>();
        var brand = await _brandRepository.GetById(id);

        if (brand == null)
        {
            response.Errors.Add("Brand Not Found");
            return NotFound(response);
        }

        response.Data = brand;
        
        return Ok(response);
    }
    
    [HttpPut]
    public async Task<ActionResult<Response<Brand>>> Update([FromBody] Brand brand)
    {
        var response = new Response<Brand>
        {
            Data = await _brandRepository.UpdateAsync(brand)
        };
        return Ok(response);
    }
    
    [HttpDelete]
    [Route("{id:int}")]
    public async Task<ActionResult<Response<bool>>> Delete(int id)
    {
        var response = new Response<bool>()
        {
            Data = await _brandRepository.DeleteAsync(id)
        };
        return Ok(response);
    }
}