using DeLaSalle.Ecommerce.Core.Dto;

namespace DeLaSalle.Ecommerce.Api.Services.Interfaces;

public interface IProductCategoryService
{
    Task<bool> ProductCategoryExist(int id);
    Task<ProductCategoryDto> SaveAsync(ProductCategoryDto categoryDto);
    Task<ProductCategoryDto> UpdateAsync(ProductCategoryDto categoryDto);
    Task<List<ProductCategoryDto>> GetAllAsync();
    Task<bool> DeleteAsync(int id);
    Task<ProductCategoryDto> GetById(int id);
    Task<bool> ExistByName(string name, int id = 0);
}