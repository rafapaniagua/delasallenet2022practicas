using System.Data.Common;
using DeLaSalle.Ecommerce.Api.DataAccess.Interfaces;
using MySqlConnector;

namespace DeLaSalle.Ecommerce.Api.DataAccess;

public class DbContext : IDbContext
{
    private readonly IConfiguration _config;
    
    private readonly string _connectionString = "server=localhost;user=root;pwd=Caballo1*;database=Ecommerce;port=3306";

    public DbContext(IConfiguration config)
    {
        _config = config;
    }
    
    private MySqlConnection _connection;

    public DbConnection Connection
    {
        get
        {
            if (_connection == null)
            {
                _connection = new MySqlConnection(_config.GetConnectionString("DefaultConnection"));
            }
            
            return _connection;
        }
    }
    
    /*public DbConnection Connection
    {
        get
        {
            if (_connection == null)
            {
                _connection = new MySqlConnection(_connectionString);
            }
            
            return _connection;
        }
    }*/
    
    
}