
CREATE SCHEMA IF NOT EXISTS `Ecommerce` ;

USE `Ecommerce` ;

CREATE TABLE `Brand` 
(
   `Id` int NOT NULL AUTO_INCREMENT,
   `Name` varchar(45) DEFAULT NULL,
   `Description` varchar(100) DEFAULT NULL,
   `IsDeleted` tinyint(1) DEFAULT '0',
   `CreatedBy` varchar(45) DEFAULT NULL,
   `CreatedDate` datetime DEFAULT CURRENT_TIMESTAMP,
   `UpdatedBy` varchar(45) DEFAULT NULL,
   `UpdatedDate` datetime DEFAULT NULL,
   PRIMARY KEY (`Id`)
) ENGINE=InnoDB ;